with open(file="coverage.xml") as file:
  file_content = file.readlines()

line_rate = None

for line in file_content:
  try:
    line = line.strip().split("<")[1].split(">")[0].split(" ")
    if line[0] == "package":
      line_rate = "Code coverage: " + str(float(line[2].split("=\"")[1].split("\"")[0])*100) + "%"
  except:
    pass

print(line_rate)